import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'list_form.dart';
import 'list_inform.dart';
import 'login_widget.dart';
import 'movie.dart';

class Homepage extends StatelessWidget {
  const Homepage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
     Widget HomeMenu = Container(
      padding: const EdgeInsets.only(top: 350),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                /*2*/
                Padding(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Coming Soon",
                      style: TextStyle(
                          color: Colors.yellow,
                          fontSize: 50,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Of'),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 40),
                  child: Align(
                    alignment: Alignment.center,
                    child: Text(
                      "Movie",
                      style: TextStyle(
                          color: Colors.yellow,
                          fontSize: 50,
                          fontWeight: FontWeight.w800,
                          fontFamily: 'Of'),
                    ),
                  ),
                ),
                  Padding(
                  padding: const EdgeInsets.only(bottom: 20),
                  child:
                TextButton(
                  
                  child: Text("Continue".toUpperCase(),
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w800)),
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.only(
                              left: 100, bottom: 22, top: 22, right: 100)),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.yellow),
                      foregroundColor: MaterialStateProperty.all<Color>(
                          Colors.blue.shade800),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.yellow)))),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => Coming()),
                    );
                  },
                )),
                
                TextButton(
                  child: Text("My List".toUpperCase(),
                      style:
                          TextStyle(fontSize: 14, fontWeight: FontWeight.w800)),
                  style: ButtonStyle(
                      padding: MaterialStateProperty.all<EdgeInsets>(
                          EdgeInsets.only(
                              left: 50, bottom: 22, top: 22, right: 50)),
                      backgroundColor:
                          MaterialStateProperty.all<Color>(Colors.yellow),
                      foregroundColor: MaterialStateProperty.all<Color>(
                          Colors.blue.shade800),
                      shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                          RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.yellow)))),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => MyList(
                              title: 'My List',
                              style: TextStyle(
                                  color: Colors.yellow,
                                  fontSize: 50,
                                  fontWeight: FontWeight.w800,
                                  fontFamily: 'Of',
                                  backgroundColor: Color(0xff0019FF),))),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
    return Scaffold(
          backgroundColor: Color(0xff0019FF),
      appBar: AppBar(
         backgroundColor: Color(0xff0019FF),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.exit_to_app),
                color: Colors.white,
                onPressed: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => LoginWidget()),
                    );
                  },)
          ],
      ),
      body:ListView(children: [
             HomeMenu
          ]),
          
        );
        
    
  }
  
}

class MyList extends StatefulWidget {
  MyList({Key? key, required this.title, required TextStyle style})
      : super(key: key);
  final String title;

  @override
  _MyList createState() => _MyList();
}

class _MyList extends State<MyList> {
  CollectionReference lists =
      FirebaseFirestore.instance.collection('collections');
  Future<void> addList() {
    return lists
        .add({'Name': 'Earnearn'})
        .then((value) => print('User Added'))
        .catchError((error) => print('Failed to add user: $error'));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
         backgroundColor: Color(0xff0019FF),
          actions: <Widget>[
            IconButton(
                icon: Icon(Icons.exit_to_app),
                color: Colors.white,
                onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => LoginWidget()));
                  })
          ],
         title: Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                "My List",
                style: TextStyle(
                  color: Colors.yellow,
                  fontSize: 28,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
      ),
      body: Center(child: ListInform()),
     // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
  
}

