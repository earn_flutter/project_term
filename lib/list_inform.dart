import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coming_soon/itaewon.dart';
import 'package:coming_soon/list_form.dart';
import 'package:flutter/material.dart';
 
class ListInform extends StatefulWidget {
  @override
  _ListInformState createState() => _ListInformState();
}

class _ListInformState extends State<ListInform> {
  
   late final Stream<QuerySnapshot> _collections =
      FirebaseFirestore.instance.collection('collections').where('Status', isEqualTo: true).snapshots();
  CollectionReference lists = FirebaseFirestore.instance.collection('collections');
   Future<void> updateUser(id) {
    return lists
        .doc(id)
        .update({
          'Status': false,
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: _collections,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong');
        }
        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            return ListTile(
              title: Text(data['Name']),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => Itaewon_more()));
              },
              
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  await updateUser(document.id);
                },
                
              ),
              
            );
          }).toList(),
        );
      },
    );
  }
}
