import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coming_soon/itaewon.dart';
import 'package:flutter/material.dart';
import 'DP.dart';
import 'Truebeauty.dart';
import 'beyond.dart';
import 'homepage.dart';
import 'hometown.dart';
import 'login_widget.dart';
import 'moon.dart';
import 'never.dart';

class Coming extends StatelessWidget {
  Coming({Key? key}) : super(key: key);
  CollectionReference lists =
      FirebaseFirestore.instance.collection('collections');
  late final Stream<QuerySnapshot> _collections =
      FirebaseFirestore.instance.collection('collections').snapshots();
  Future<void> updateUser(id) {
    return lists
        .doc(id)
        .update({
          'Status': true,
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff0019FF),
        title: Align(
          alignment: Alignment.bottomLeft,
          child: Text(
            "Coming Soon",
            style: TextStyle(
              color: Colors.yellow,
              fontSize: 28,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            color: Colors.white,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginWidget()),
              );
            },
          )
        ],
      ),
      body: StreamBuilder(
          stream: _collections,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading');
            }
            return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String?, dynamic> data =
                    document.data()! as Map<String?, dynamic>;
                // return Text('${data['Name']} ${data['Date']} ${data['Pic']}');
                return Container(
                  child: Column(
                    children: [
                      Image.asset(
                        data['Pic'],
                        width: 390,
                        height: 220,
                        fit: BoxFit.cover,
                      ),
                      Container(
                        padding: const EdgeInsets.only(
                            right: 10, left: 10, top: 10, bottom: 30),
                        child: Row(
                          children: [
                            Expanded(
                                child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                    child: Text('${data['Name']}',
                                  style: TextStyle(
                                      fontSize: 22,
                                      fontWeight: FontWeight.w900,
                                      fontFamily: 'Of'),
                                )),
                                Text(data['Date'],
                                    style: TextStyle(color: Colors.grey[500]))
                              ],
                            )),
                            IconButton(
                              icon: const Icon(Icons.favorite),
                              tooltip: 'add MyList',
                              onPressed: () async {
                                await updateUser(document.id);
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => MyList(
                                            title: 'My List',
                                            style: TextStyle(
                                              color: Colors.yellow,
                                              fontSize: 50,
                                              fontWeight: FontWeight.w800,
                                              fontFamily: 'Of',
                                              backgroundColor:
                                                  Color(0xff0019FF),
                                            ))));
                              },
                            ),
                            IconButton(
                              icon: const Icon(Icons.info),
                              tooltip: 'more',
                              onPressed: () {
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => Itaewon_more()),
                                );
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }).toList(),
            );
          }),
    );
  }
}
