import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class ListForm extends StatefulWidget {
  String listsId;
  ListForm({Key? key, required this.listsId}) : super(key: key);

  @override
  _ListFormState createState() => _ListFormState(listsId);
}

class _ListFormState extends State<ListForm> {
  String listsId = '';
  String Name = '';

  CollectionReference lists =
      FirebaseFirestore.instance.collection('collections');
  _ListFormState(this.listsId);
  TextEditingController _NameController = new TextEditingController();

  @override
  void initState() {
    super.initState();
    if (this.listsId.isNotEmpty) {
      lists.doc(this.listsId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data()! as Map<String, dynamic>;
          Name = data['Name'];
          _NameController.text = Name;
        }
      });
    }
  }

  final _forKey = GlobalKey<FormState>();

  Future<void> addUser() {
    return lists
        .add({
          'Name': this.Name,
        })
        .then((value) => print("User Added"))
        .catchError((error) => print("Failed to add user: $error"));
  }

  Future<void> updateUser() {
    return lists
        .doc(this.listsId)
        .update({
          'Name': Name,
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff0019FF),
        title: Align(
          alignment: Alignment.bottomLeft,
          child: Text(
            "My List",
            style: TextStyle(
              color: Colors.yellow,
              fontSize: 28,
              fontWeight: FontWeight.w800,
            ),
          ),
        ),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _forKey,
        child: Column(
          children: [
            TextFormField(
              controller: _NameController,
              decoration: InputDecoration(labelText: 'Name'),
              onChanged: (value) {
                setState(() {
                  Name = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return 'Plase input Name';
                }
                return null;
              },
            ),
            ElevatedButton(
                onPressed: () async {
                  if (_forKey.currentState!.validate()) {
                    if (listsId.isEmpty) {
                      await addUser();
                    } else {
                      await updateUser();
                    }
                    Navigator.pop(context);
                  }
                },
                child: Text("Save",
                    style:
                        TextStyle(fontSize: 15, fontWeight: FontWeight.w800)),
                style: ButtonStyle(
                    padding: MaterialStateProperty.all<EdgeInsets>(
                        EdgeInsets.only(
                            left: 35, bottom: 20, top: 20, right: 35)),
                    backgroundColor:
                        MaterialStateProperty.all<Color>(Colors.blue.shade800),
                    foregroundColor:
                        MaterialStateProperty.all<Color>(Colors.yellow),
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side:
                                BorderSide(color: Colors.lightBlue.shade900)))))
          ],
        ),
      ),
    );
  }
}
