import 'package:flutter/material.dart';

class Beyond_more extends StatelessWidget {
  const Beyond_more({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
  Widget itaewon = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  
                  child: Text(
                    'ความยาวหนัง : 16 Ep',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
              Text('ผู้กำกับ : คิมซองยุน',
                   style: TextStyle(fontWeight: FontWeight.bold)),
                   Text('ผู้เขียนบท : โจกวังจิน',
                   style: TextStyle(fontWeight: FontWeight.bold)),
                    Text('นักแสดงหลัก : พัคซอจุน,คิมดามิ,ยูแจมยอง,ควอนนารา',
                   style: TextStyle(fontWeight: FontWeight.bold)),
                   Text('เรื่องย่อ : Itaewon Class (อิแทวอน) ซีรีส์ที่สร้างจากเว็บตูน'
'เรตติ้ง 9.9 ใน Daum Webtoon! ซีรีส์เรื่องนี้ว่าด้วยเรื่องราว'
'ของ พัคแซรอย(รับบทโดย พัคซอจุน) ชายหนุ่มผู้กล้าเผชิญ'
'หน้ากับความอยุติธรรมอย่างไม่ประนีประนอม จนทำให้ชีวิต'
'ประสบกับความตกต่ำและสูญเสียคุณพ่อจากอุบัติเหตุ'
'เขาตัดสินใจลุกขึ้นสู้ด้วยการทำธุรกิจร้านอาหารในย่าน'
'อิแทวอน โดยเป้าหมายการทำธุรกิจของ พัคเซรอยคือ'
'การแก้แค้นการต่อสู้บนเส้นทางที่ไม่ได้โรยด้วยกลีบกุหลาบ'
'ที่มาพร้อมกับความแค้นของเขาจะเป็นอย่างไร'
 'ติดตามได้ใน Itaewon Class (2020) : ธุรกิจปิดเกมแค้น',
                   style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          )),
        ],
      ),
    );

    
    return Scaffold(
      appBar: AppBar(
         backgroundColor: Color(0xff0019FF),
         title: Align(
              alignment: Alignment.bottomLeft,
              child: Text(
                "Beyond Evil",
                style: TextStyle(
                  color: Colors.yellow,
                  fontSize: 28,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
      ),
      body:ListView(children: [
      
            Image.asset(
              'images/beyond.jpg',
              width: 322,
              height: 500,
              fit: BoxFit.cover,
            ),
            itaewon,
             
          ]),
          
        );
        
  }
}

        
