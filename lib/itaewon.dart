import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:coming_soon/login_widget.dart';
import 'package:flutter/material.dart';

class Itaewon_more extends StatelessWidget {
  Itaewon_more({Key? key}) : super(key: key);
 CollectionReference lists =
      FirebaseFirestore.instance.collection('collections');
  late final Stream<QuerySnapshot> _collections =
      FirebaseFirestore.instance.collection('collections').snapshots();
  Future<void> updateUser(id) {
    return lists
        .doc(id)
        .update({
          'Status': true,
        })
        .then((value) => print("User Updated"))
        .catchError((error) => print("Failed to update user: $error"));
  }
  
  @override
  Widget build(BuildContext context) {
  return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(0xff0019FF),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.exit_to_app),
            color: Colors.white,
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => LoginWidget()),
              );
            },
          )
        ],
      ),
      body: StreamBuilder(
          stream: _collections,
          builder:
              (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
            if (snapshot.hasError) {
              return Text('Error');
            }
            if (snapshot.connectionState == ConnectionState.waiting) {
              return Text('Loading');
            }
            return ListView(
              children: snapshot.data!.docs.map((DocumentSnapshot document) {
                Map<String?, dynamic> data =
                    document.data()! as Map<String?, dynamic>;
                // return Text('${data['Name']} ${data['Date']} ${data['Pic']}');
                return
                Container(
                  child: Column(
                    children: [
                      Image.asset(
                        data['More_Pic'],
                          width: 322,
              height: 500,
              fit: BoxFit.cover,
                      
                      ),
                       Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            
            children: [
              Container(
                  
                  child: Text(
                    'ความยาวหนัง : 16 Ep',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
              Text('ผู้กำกับ : คิมซองยุน',
                   style: TextStyle(fontWeight: FontWeight.bold)),
                   Text('ผู้เขียนบท : โจกวังจิน',
                   style: TextStyle(fontWeight: FontWeight.bold)),
                    Text('นักแสดงหลัก : พัคซอจุน,คิมดามิ,ยูแจมยอง,ควอนนารา',
                   style: TextStyle(fontWeight: FontWeight.bold)),
                   Text('เรื่องย่อ : Itaewon Class (อิแทวอน) ซีรีส์ที่สร้างจากเว็บตูน'
'เรตติ้ง 9.9 ใน Daum Webtoon! ซีรีส์เรื่องนี้ว่าด้วยเรื่องราว'
'ของ พัคแซรอย(รับบทโดย พัคซอจุน) ชายหนุ่มผู้กล้าเผชิญ'
'หน้ากับความอยุติธรรมอย่างไม่ประนีประนอม จนทำให้ชีวิต'
'ประสบกับความตกต่ำและสูญเสียคุณพ่อจากอุบัติเหตุ'
'เขาตัดสินใจลุกขึ้นสู้ด้วยการทำธุรกิจร้านอาหารในย่าน'
'อิแทวอน โดยเป้าหมายการทำธุรกิจของ พัคเซรอยคือ'
'การแก้แค้นการต่อสู้บนเส้นทางที่ไม่ได้โรยด้วยกลีบกุหลาบ'
'ที่มาพร้อมกับความแค้นของเขาจะเป็นอย่างไร'
 'ติดตามได้ใน Itaewon Class (2020) : ธุรกิจปิดเกมแค้น',
                   style: TextStyle(fontWeight: FontWeight.bold)),
            ],
          )),
        ],
      ),
    )]));

              }).toList(),
            );
          }),
    );
        
  }
}

        
